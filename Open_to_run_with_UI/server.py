#importing all the required modules
import requests
import json
from flask import Flask, request, Response
import flask
from flask import render_template, Flask, request, Response, jsonify
from flask_cors import CORS, cross_origin
from collections import OrderedDict

#Flask app
app = Flask(__name__)

#Testing APIs
@app.route('/')
@app.route('/index')
def index():
	return "Hello, World!"


#Function to get Top "m" contributors of a repository "rep"
def get_m_contributors(rep,m):
	pg=1 #variable to keep the track of page number
	url_call="https://api.github.com/repos"+rep+"contributors?page="+str(pg)#URL for quering the first page of contributors of "rep"
	contributors=url_call
	contributors_list=OrderedDict() #dictionary which will keep mapping [username]->[contribution]
	x = requests.get(contributors,headers={"User-Agent":"Mozilla/5.0"}) #Quering the first page of contributors of "rep"
	y = json.loads(x.text)# loading the response
	while y and len(contributors_list)<m:	#Will loop till we get m contributors or all the contributor have been considered 		
		for i in y:
			contributors_list[i['login']]=i['contributions'] #storing the required mapping
		pg+=1 #incrementing page number
		url_call="https://api.github.com/repos"+rep+"contributors?page="+str(pg) #changing the url based on page number
		x = requests.get(url_call,headers={"User-Agent":"Mozilla/5.0",'Authorization': 'token '+'b628eaed44e778167b5ad74913420668b67059ba'})#Quering the further pages of contributors of "rep"
		y = json.loads(x.text)# loading the response
	final_list=[] #final list to hold [[usename,contributions]]
	for i in contributors_list:
		final_list.append((i,contributors_list[i])) #building the final lists
	return final_list[:m] #returning the first m elements of the final list

#Function to get a mapping of n repositories as ["repository"]-->[List of top m contributors] for "company"
def get_n_repositories(company,n,m):
	pg=1 #variable to keep track of page numbers
	url_call="https://api.github.com/orgs/"+company+"/repos?page="+str(pg)#URL for quering the first page of repositories of "company"
	print(url_call)
	repositories=url_call
	repositories_dict={} #dictionary to map [name of remo]-->[number of forks]
	x = requests.get(repositories,headers={"User-Agent":"Mozilla/5.0"})#Quering the first page of repositories of "company"
	y = json.loads(x.text) #loading the response
	cnt=1 #counting the total number of pages
	while y and cnt<60:	# loop till we are recieving repositories	
		for i in y:
			repositories_dict[i['full_name']]=i['forks'] #creating the required mapping
		pg+=1 #incrementing page number
		url_call="https://api.github.com/orgs/"+company+"/repos?page="+str(pg)#changing the url based on page number
		cnt+=1
		x = requests.get(url_call,headers={"User-Agent":"Mozilla/5.0",'Authorization': 'token '+'b628eaed44e778167b5ad74913420668b67059ba'})#Quering the further pages of repositories of "company"
		y = json.loads(x.text) #loading the response
	final_repo_list=[] # final list containing [numberof forks,name of repo]
	for i in repositories_dict:
		final_repo_list.append((repositories_dict[i],i))# building final_repo_list
	final_repo_consider= sorted(final_repo_list,reverse=True)[:n] #sorting and taking first n repositories based on number of forks
	final_res=[] #final result to be returned [reponame+forks,top m contributors]
	for i in final_repo_consider:
		final_res.append((i[1],'+',repositories_dict[i[1]],'+',get_m_contributors("/"+i[1]+"/",m))) #calling get_m_contributors for each of the n repositories
		final_res.append('*') #creating output for easy formating in frontend
	return final_res #returning the final result


# The main endpoint (API)
@app.route('/api/repos', methods=['POST'])
def find_repo():
	file = request.data #getting the data
	print(file)#debugging logs
	try:		#looking for the correctness of the input
		file=str(file)[3:-2]
		l=file.split(',') #splitting input to get all the three inputs
		print(l)
		company=l[0].split(':')[1][1:-1] #getting company name
		m=int(l[1].split(':')[1][1:-1]) #getting the value of m
		n=int(l[2].split(':')[1][1:-1]) #getting the value of n
		print(company,m,n) #debugging logs
		res=get_n_repositories(company.lower(),m,n) #callin the get_n_repositories function to generate the output
		return str(res),200 #returning the output with code 200
	except: #return 400 if input is not correct
		print("Error")
		return {},400


#main function
if __name__ == '__main__':
	app.debug = True
	cors = CORS(app) #for cross origin access
	app.config['CORS_HEADERS'] = 'application/json' #setting the header
	app.run('0.0.0.0', port=8000) #to run on localhost port 8000
	http_server.serve_forever()