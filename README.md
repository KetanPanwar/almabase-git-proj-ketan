# Almabase_git_proj
Find the n most popular repositories of a given organization on Github based on the number of forks. For each such repo find the top m committees and their commit counts. 

To run on your system
Fork this repo on a system which has python(any version).

Open the folder throught which you want to run the application.
Next follow the instructions in the "instructions.txt" file in the respective folder.

Assumptions and points to be taken care of:-
-The application might work slow for the companies with more than 1500 repositories. (But will give the correct output)
-Using the application very frequently might have you run out of github API hits. So User is suggested to keep values of m and n in the range of (1-50).
-If you are out of github API hits, wait for some time and then try again.

*The above problems can be solved by using a cache to store the response data frequently.